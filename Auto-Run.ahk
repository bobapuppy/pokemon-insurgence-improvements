#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force

; NOTE: In autohotkey, the following special characters (usually) represent modifier keys:
; # is the WIN key. (it can mean other things though, as you can see above.)
; ^ is CTRL
; ! is ALT
; + is SHIFT
; list of other keys: http://www.autohotkey.com/docs/Hotkeys.htm

#MaxThreadsPerHotkey 8


MainWindow := 6969
CurrentWindow := 69


; Reload button.
^!r::
Send {RShift up}
Reload


; Sets the window ID that the script is looking for.
Del::
MainWindow := WinExist("A")
Return


; Main script.
Ins::
Loop
{
	CurrentWindow := WinExist("A")
	Sleep, 250
	if MainWindow = %CurrentWindow%
	{
		Send {RShift down}
	}
	else
	{
		GetKeyState, state, Shift
		if state = D
		{
			Send {RShift up}
		}
	}
}
Return